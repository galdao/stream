# Desafio Stream #

O código nesse repositório é uma solução **O(n)** para o seguinte problema:

Utilizando uma interface Stream com os métodos hasNext() e getNext(), crie um método que retorna o primeiro char único em uma String que é antecedido por uma consoante e uma vogal.

**Exemplo:**

Input: aAbBABacafexCviRoe

Output: o

No exemplo, ‘o’ é o primeiro caractere Vogal da stream que não se repete após a primeira e tem como antecessores uma consoante ‘R’ e uma vogal ‘i’.

### Como usar ###

* Clonar o repositório
* Na linha de comando, dentro da pasta do repositório, rodar o comando: *mvn clean package*
* Entrar na pasta target
* Rodar o comando: *java -jar stream.jar **String_da_Stream***

### Requerimentos ###

* Java 8
* Maven