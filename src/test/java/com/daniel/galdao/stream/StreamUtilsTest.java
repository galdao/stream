package com.daniel.galdao.stream;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by IFC.Daniel on 13/07/2017.
 */
@RunWith(JUnitParamsRunner.class)
public class StreamUtilsTest {

    @Test
    @Parameters(method = "firstCharTestData")
    public void firstCharTest(String input, char result, Class<? extends Exception> expectedException) throws Exception {
        try {
            Assert.assertEquals(StreamUtils.firstChar(new StringStream(input)), result);
            if (expectedException != null){
                Assert.fail();
            }
        } catch (Exception e){
            if (expectedException == null || !e.getClass().equals(expectedException)){
                throw e;
            }
        }
    }

    public Object[][] firstCharTestData(){
        return new Object[][]{
                {"abcdefi", 'i', null}, //Caso simples
                {"asdasdasda", ' ', IllegalArgumentException.class}, //Caso sem solução
                {"adEFigohu", 'E', null}, //Confere se a primeira ocorrencia é retornada
                {"adefigohuei", 'o', null}, //Confere se repeticoes ao final da Stream sao levadas em conta
                {"ad123/,/.,;qwefigo1239801712ahui.,/o.,/", 'u', null}, //Confere se caracteres especiais nao quebram a logica
                {"a1e/i|oFU", 'U', null}, //Confere se caracteres especiais nao sao considerados consoantes
                {"afedegehi", 'i', null}// Confere se uma vogal que atende os requisitos em todas as suas aparicoes ainda é eliminada
        };
    }

}