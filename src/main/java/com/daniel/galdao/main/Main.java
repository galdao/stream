package com.daniel.galdao.main;

import com.daniel.galdao.stream.Stream;
import com.daniel.galdao.stream.StreamUtils;
import com.daniel.galdao.stream.StringStream;

/**
 * Created by IFC.danielgaldao on 13/07/17.
 */
public class Main {

    public static void main(String[] args) {
        if (args.length == 0 || args.length > 1){
            System.out.println("USAGE: java -jar stream.jar string");
            System.exit(1);
        }
        Stream stream = new StringStream(args[0]);
        Character firstChar = null;
        try {
            firstChar = StreamUtils.firstChar(stream);
        } catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
            System.exit(1);
        }
        System.out.println(String.format("First Char: %s", firstChar));
    }

}
