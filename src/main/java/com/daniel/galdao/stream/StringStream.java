package com.daniel.galdao.stream;

/**
 * Created by IFC.danielgaldao on 13/07/17.
 */
public class StringStream implements Stream {

    private char[] stringChars;
    private int position;

    public StringStream(String input) {
        stringChars = input.toCharArray();
        position = 0;
    }

    public char getNext() {
        return stringChars[position++];
    }

    public boolean hasNext() {
        return position < stringChars.length;
    }
}
