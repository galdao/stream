package com.daniel.galdao.stream;

/**
 * Created by IFC.danielgaldao on 13/07/17.
 */
public interface Stream {

    char getNext();

    boolean hasNext();

}
