package com.daniel.galdao.stream;

import java.util.*;

/**
 * Created by IFC.danielgaldao on 13/07/17.
 */
public class StreamUtils {

    //vogais e consoantes definidas em Sets para tirar vantagem do fato que a busca em um Set eh O(1)
    private static Set<Character> vowels = new HashSet<>(Arrays.asList('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'));
    private static Set<Character> consonant = new HashSet<>(Arrays.asList('b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z',
                                                                          'B', 'C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'X', 'Y', 'Z'));

    /**
     * Método que retorna o primeiro char único em uma String que é antecedido por uma consoante e uma vogal com complexidade O(n) sendo n o número de letras na String.
     *
     * @param stream Stream de uma String
     * @return Primeiro char único na String que é antedecido por uma consoante e uma vogal.
     */
    public static char firstChar(Stream stream) {
        Set<Character> recurringVowels = new HashSet<>(); //set usado para conferir vogais repetidas
        Set<Character> possibleChars = new LinkedHashSet<>(); //set de caracteres que cumpriram os requisitos do problema em dado ponto da execuçao da stream, LinkedHashSet é usado para mantera ordem das entradas
        boolean wasLastVowel = false; //usado para sinalizar se a letra anterior da stream era uma vogal
        boolean areConditionsMet = false; //usado para sinalizar se a letra anterior era uma consoante e a anterior a ela uma vogal
        while (stream.hasNext()) {
            char currentChar = stream.getNext();
            if (vowels.contains(currentChar)) {
                if (!recurringVowels.contains(currentChar)) {
                    if (areConditionsMet && !possibleChars.contains(currentChar)) {
                        possibleChars.add(currentChar); //Adicionar ao fim de um LinkedHashSet tem complexidade O(1)
                    } else {
                        recurringVowels.add(currentChar); //Adicionar ao fim de um HashSet tem complexidade O(1)
                    }
                }
                wasLastVowel = true;
                areConditionsMet = false;
            } else if(consonant.contains(currentChar)) {
                areConditionsMet = wasLastVowel;
                wasLastVowel = false;
            } else {
                areConditionsMet = false;
                wasLastVowel = false;
            }
        }
        for (char possibleChar : possibleChars) {
            if (!recurringVowels.contains(possibleChar)) { //se a vogal aparecer no Set de vogais repetidas, ela não é mais uma resposta válida
                return possibleChar;
            }
        }
        throw new IllegalArgumentException("No first char found");
    }

}
